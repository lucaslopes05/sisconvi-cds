const express = require('express')
const app = express()
const consign  = require('consign')
const handlebars = require('express-handlebars')


//Tamplete Engine 
app.engine('handlebars',handlebars({defaultLayout: 'main'}))
app.set('view engine','handlebars')

// Carregamento das Routes,
consign()
.include('routes')
.into(app)

//Outros 
    //numero da posta da aplicacao 
    const PORT = 3000
    app.listen(PORT, ()=>{
        console.log("Servidor rodano... ")
    })